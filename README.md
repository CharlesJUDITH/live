# Liste des Lives et sujets

## Lives passés

### ZABBIX

- Agent ZABBIX - Actif vs Passif: le Mardi 7 Avril 2020
- CACTI vs ZABBIX: le Jeudi 9 Avril 2020
- Les LLD avec ZABBIX: le Jeudi 23 Avril 2020



### Nouvelle version de ZABBIX

- ZABBIX 5.0: le Mardi 21 Avril 2020



### Sujet à aborder

- ZABBIX, GIT et Ansible AWX
- Le SNMP avec ZABBIX
-
